wx.cloud.init();
// url
var urls={
    login:"login",
    sum:"sum",
    getGoodsItems:"getGoodsItems",
    changeCollectionStatus:"changeCollectionStatus",
    addOrders:"addOrders",
    addComments:"addComments",
    getMyCollections:"getMyCollections",
    addAddr:"addAddr",
    goodsItems:"goodsItems"
};
// methods
var get =  (name, data)=> {
    return wx.cloud.callFunction({
        name: name,
        data: "" || data
    });
};

module.exports = {
    urls:urls,
    get: get
};
