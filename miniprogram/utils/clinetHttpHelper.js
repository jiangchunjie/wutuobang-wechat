class ClientHttpHelper {

    constructor() {
        wx.cloud.init();
        let DB = wx.cloud.database({
            env: 'wutuobang-cloud-e5bc01'
        });
        this.DB = DB;
        this._ = DB.command;
    }

    // 登录
    login(username, password) {
        let dataBase = this.DB;
        let users = dataBase.collection("users");
        return users.where({
            username: username,
            password: password
        }).get();
    }

    // 获取订单列表
    getOrdersList(uid, type, index, per) {
        let ordersList = this.DB.collection("orders");
        var parem = {uid: uid};
        if (type) {
            parem.orders_status = type;
        }
        return ordersList.where(parem).skip(per * index).limit(per).get()
    }

    //获取某个商品
    getSomeGoods(id) {
        let ordersList = this.DB.collection("goodsItems");
        var parem = {_id: id};
        return ordersList.where(parem).get()
    }

    //判断是否收藏
    isCollection(uid) {
        let ordersList = this.DB.collection("users");
        var parem = {_id: uid};
        return ordersList.where(parem).get()
    }


    // 页面
    // 个人中心数据
    getPersonPageDatas(uid) {
        var data = {};
        var users = this.DB.collection("users");
        return users.doc(uid).get();
    }

    // 个人中心数据
    getPersonCollections(uid) {
        var that = this;
        var data = {};
        var users = this.DB.collection("users");
        var goods = this.DB.collection("goodsItems");
        return new Promise((resolve, reject) => {
            users.doc(uid).get()
                .then(res => {
                    var arr = res.data.likes;
                    var _ = this.DB.command;
                    goods.where({
                        id: _.in(arr)
                    }).get()
                        .then(res => {
                            resolve(res)
                        })
                    // arr.forEach( (item, index) => {
                    //      goods.doc(item).get()
                    //          .then(res=>{
                    //              datas.push(res.data);
                    //              if (index===arr.length-1) {
                    //                  resolve(datas);
                    //              }
                    //          })
                    //
                    // })
                })
        })
    }

    //获取我的店铺数据
    getMyShopPageInfo(uid,type,page,per) {
        var goods = this.DB.collection("goodsItems");
        if (type==='all') {
            return goods.where({
                uid: uid
            }).skip((page-1)*per).limit(per).get();
        }else{
            return goods.where({
                uid: uid,
                type:type
            }).skip((page-1)*per).limit(per).get();
        }

    }


}

module.exports = {
    ClientHttpHelper: ClientHttpHelper
};
