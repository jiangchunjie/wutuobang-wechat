const formatTime = (date,n=0) => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()+n
    const minute = date.getMinutes()
    const second = date.getSeconds()

    return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
};

//获取时间日期
const myFormatDate=(date) => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()

    return [year, month, day].map(formatNumber).join('-')
};
const myFormatTime=(date,n=0) => {

    const hour = date.getHours()+n
    const minute = date.getMinutes()

    return [hour, minute].map(formatNumber).join(':')
};


// 获取两个时间的产值
const calculateTime=(start,end)=>{
    let times=new Date(end).getTime() -new Date(start).getTime();
    return (times/1000/3600).toFixed(2);
}

const compare=(sd,st,ed,et)=>{
    return (new Date(sd+" "+st)<new Date(ed+" "+et))
}
const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
};

module.exports = {
    formatTime: formatTime,
    myFormatDate:myFormatDate,
    myFormatTime:myFormatTime,
    calculateTime:calculateTime,
    compare:compare
};
