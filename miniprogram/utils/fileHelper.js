let upload=(aimPath,tempPath)=>{
    return wx.cloud.uploadFile({
        cloudPath: aimPath,
        filePath: tempPath, // 文件路径
    })
};
let getTempFileURL=(id)=> {
    return wx.cloud.getTempFileURL({
        fileList: [{
            fileID: id,
            maxAge: 60 * 60, // one hour
        }]
    });
};
module.exports={
    upload:upload,
    getTempFileURL:getTempFileURL
};
