import Util from './../../utils/util';

import HttpServer from './../../utils/serverCloud';

const app = getApp();
Page({

    /**
     * 页面的初始数据
     */
    data: {
        gid: "",
        numEverWrite: 0,
        message: "",
        type: 0//0匿名还是1实名？
    },
    bindInput(e) {
        this.setData({
            numEverWrite: e.detail.value.length,
            message: e.detail.value
        })
    },
    writeOK() {
        var that = this;
        if (this.data.numEverWrite <= 5) {
            wx.showToast({
                title: '评论的长度不能小于五个字！'
                , icon: "none"
            });
            return;
        }
        var params = {};
        params.date = Util.formatTime(new Date());
        params.message = this.data.message;
        params.type = this.data.type;
        params.photoUrl = app.globalData.userInfo.avatarUrl;

        wx.getStorage({
            key: 'uid',
            success: res => {
                params.uid = res.data;
                var data = {
                    gid: that.data.gid,
                    params: params
                };
                console.log(data)
                HttpServer.get(HttpServer.urls.addComments, data)
                    .then(res => {
                        if (res.result.stats.updated > 0) {
                            wx.showModal({
                                title: '评论成功！',
                                content: '评论OK',
                                confirmText: "返回",
                                showCancel: false,
                                success: res => {
                                    if (res.confirm) {
                                        wx.navigateBack()
                                    }
                                }
                            })
                        }
                    })
            }
        });
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        if (options.type === '0') {//匿名的评论
            wx.setNavigationBarTitle({
              title: '匿名评论'
                // console.lo
            })
        }
        if (options.gid) {
            this.setData({
                gid: options.gid || "f1b9d07166a61e05c",
                type: options.type || "f1b9d07166a61e05c"
            })
        }
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
