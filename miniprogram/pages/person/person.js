// pages/person/person.js
const app = getApp()
import ClientHttp from "./../../utils/clinetHttpHelper"

let clientHttp = new ClientHttp.ClientHttpHelper();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        userData: {},
        userId: "",
        userInfo: {}//获得数据
    },
    goOrdersList() {
        wx.navigateTo({
            url: '/pages/personOrderList/personOrderList'
        })
    },
    logout() {
        wx.showModal({
            title: '正在退出登录！',
            content: '退出登录',
            success: res => {
                if (res.confirm) {
                    wx.redirectTo({
                        url: '/pages/login/login'
                    })
                }
            }
        })
    },
    goCollects() {
        wx.navigateTo({
            url: '/pages/personCollection/personCollection'
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        wx.getStorage({
            key: 'uid',
            success: res => {
                clientHttp.getPersonPageDatas(res.data)
                    .then(res => {
                        that.setData({
                            userInfo: res.data
                        });
                    });
            }
        });
        this.setData({
            'userData': app.globalData.userInfo
        })
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        wx.getStorage({
            key: 'uid',
            success: res => {
                clientHttp.getPersonPageDatas(res.data)
                    .then(res => {
                        that.setData({
                            userInfo: res.data
                        });
                    });
            }
        });
        this.setData({
            'userData': app.globalData.userInfo
        })
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
