// pages/home/home.js
const data = require("../../utils/datas");
const http = require("../../utils/serverCloud");
const urls = http.urls;
const get = http.get;

Page({

    /**
     * 页面的初始数据
     */
    data: {
        imgUrls: [
            '../../assets/images/home-lunbo1.jpg',
            '../../assets/images/home-lunbo2.jpg',
            '../../assets/images/home-lunbo3.jpg'
        ],
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        indicatorColor: "#fff",
        indicatorActiveColor: "#1BA160",
        recomdation: {
            datas: []
        }
    },
    goDetails(e) {
        if (e.target.dataset.type === "pet") {
            wx.navigateTo({
                url: '/pages/storagePetsPage/storagePetsPage?id=' + e.target.dataset.id
            })
        }
    },


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {

        get(urls.getGoodsItems,{
            tempPagesIndex:1,
            perLength:6,
            type:'pet'
        })
            .then(res => {
                console.log(res.result.data)
                this.setData({
                    'recomdation.datas':res.result.data
                });
            });


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
