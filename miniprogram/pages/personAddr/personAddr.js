var amapFile = require('../../libs/amap-wx.js');
var myAmapFun = new amapFile.AMapWX({key: '1df6b408f60ad5c4f95d9fdb605dba47'});
import serverHttp from './../../utils/serverCloud';
import ClinetHttp from './../../utils/clinetHttpHelper';

let clinetHttp = new ClinetHttp.ClientHttpHelper();

Page({
    /**
     * 页面的初始数据
     */
    data: {
        tempPosDes: "呼和浩特",
        myAddrs: [],
        animate: {},
        showAddModal: false,
        newAddrInfo: {
            name: "",
            sex: '',//男1女二
            tel: "",
            door: "",
            tag: "",
            addr: "1",
            location: ""
        },
        isKeep: false,
        isShowDetailsPos: false,
        relatedAddress: [],

    },
    resetPosition(){
        myAmapFun.getRegeo({
            success: res => {
                that.setData({
                    tempPosDes: res[0].name,
                    relatedAddress: res[0].regeocodeData.pois
                });
            }
        });
    },
    submitAddrInfo() {
        var that = this;
        var mydata = this.data.newAddrInfo;
        if (!(mydata&&mydata.name && mydata.sex && mydata.tel && mydata.tag && mydata.addr && mydata.location)) {
            wx.showToast({
                title: '请填完信息，再提交！',
                icon: "none"
            });
            return;
        }
        if (!(/^1[34578][0-9]{9}$/.test(mydata.tel))){//171123456789
            wx.showToast({
                title: '电话号码错误！',
                icon: "none"
            });
            return;
        }
        wx.getStorage({
            key: 'uid',
            success: res => {
                serverHttp.get(serverHttp.urls.addAddr, {
                    uid: res.data,
                    addr: this.data.newAddrInfo
                }).then(res => {
                    that.setData({
                        myAddrs: that.data.myAddrs.concat(that.data.newAddrInfo),
                        newAddrInfo: null
                    });
                    that.closeAddModal();
                })
            }
        });
    },
    changeSex(e) {
        this.setData({
            'newAddrInfo.sex': e.target.dataset.sex
        })
    },
    changeTag(e) {
        this.setData({
            'newAddrInfo.tag': e.target.dataset.tag
        })
    },
    setItem(e) {
        console.log(e.detail.value)
        if (e.target.dataset.name === 'name') {
            this.setData({
                "newAddrInfo.name": e.detail.value
            })
        } else if (e.target.dataset.name === 'tel') {
            this.setData({
                "newAddrInfo.tel": e.detail.value
            })
        } else if (e.target.dataset.name === 'door') {
            this.setData({
                "newAddrInfo.door": e.detail.value
            })
        }
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;

        //恢复数据
        wx.getStorage({
            key: 'addrInfo',
            success: res => {
                that.setData({
                    newAddrInfo: res.data
                });
                wx.setStorageSync('addrInfo', null);
                //返回参数
                var searchResult = options.searchResult;
                if (searchResult) {
                    that.setData({
                        'newAddrInfo.addr': searchResult.split("|")[0],
                        'newAddrInfo.location': searchResult.split("|")[1]
                    });
                    that.alertAddModal();
                    wx.setStorage({
                        key: "searchResult",
                        data: null
                    });
                }
            }
        });


        //请求数据
        myAmapFun.getRegeo({
            success: res => {
                that.setData({
                    tempPosDes: res[0].name,
                    relatedAddress: res[0].regeocodeData.pois
                });
            }
        });
        wx.getStorage({
            key: 'uid',
            success: res => {
                clinetHttp.getPersonPageDatas(res.data)
                    .then(res => {
                        that.setData({
                            myAddrs: res.data.addrs
                        })
                    })
            }
        });

    },
    alertAddModal() {
        let animate = wx.createAnimation({
            duration: 300,
            timeFunction: 'ease'
        });
        animate.bottom("0vh").step();
        this.setData({
            showAddModal: true,
            animate: animate.export()
        });
        animate = null;
    },
    closeAddModal() {
        let animate = wx.createAnimation({
            duration: 300,
            timeFunction: 'ease'
        });
        animate.bottom("-80vh").step();
        this.setData({
            animate: animate.export(),
            showAddModal: false
        })
    },
    addrFocus() {
        this.setData({
            isKeep: true
        });
        wx.redirectTo({
            url: "/pages/search/search?originPageName=personAddr&isTab=false"
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        if (this.data.isKeep) {
            wx.setStorage({
                key: "addrInfo",
                data: this.data.newAddrInfo
            });
            console.log(this.data.newAddrInfo)
        }
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
