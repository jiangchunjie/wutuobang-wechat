import ClientHttpHelper from './../../utils/clinetHttpHelper';
import Data from './../../utils/datas';
let clientHttpHelper = new ClientHttpHelper.ClientHttpHelper();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        datas: []
    },
    goDetails(e) {
        let id = e.currentTarget.dataset.id;
        let type = e.currentTarget.dataset.type;
        console.log(type)
        let url = "";
        Data.title_urls.forEach(function (item, index) {
            if (item.title === type) {
                url = item.url
            }
        });
        wx.navigateTo({
            url: url + "?id=" + id
        })
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        wx.getStorage({
            key: 'uid',
            success: res => {
                clientHttpHelper.getPersonCollections(res.data)
                    .then(res => {

                        // if (index.photoUrl && !index.photoUrl.startsWith("http")) {
                        //     res.data[0].conments[item].photoUrl = "http:" + res.data[0].conments[item].photoUrl;
                        // }
                        console.log(res.data)
                        this.setData({
                            datas: res.data
                        })
                    })
            }
        });

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
