var amapFile = require('../../libs/amap-wx.js');
var myAmapFun = new amapFile.AMapWX({key: '1df6b408f60ad5c4f95d9fdb605dba47'});
Page({

    /**
     * 页面的初始数据
     */
    data: {
        tips:"",
        backUrl:'',
        isTab:false
    },
    nav(e){
        var that=this;
        var item=e.currentTarget.dataset.item;
        if (this.data&&this.data.isTab==="false"){
            wx.redirectTo({
              url: that.data.backUrl+"?searchResult="+item.address+item.name+"|"+item.location
            });
            return;
        }
        wx.setStorageSync('searchResult',item);
        wx.switchTab({
          url: that.data.backUrl
        });

    },
    search(e) {
        var that=this;
        if (!(e&&e.detail&&e.detail.value)) {
            return;
        }
        myAmapFun.getInputtips({
            keywords: e.detail.value,
            location: '',
            success: function(data){
                if(data && data.tips){
                    that.setData({
                        tips: data.tips
                    });
                }
            }
        })
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that=this;
        myAmapFun.getRegeo({
            success: res => {
                that.setData({
                    tips: res[0].regeocodeData.pois.slice(0,10)
                });
            }
        });
        var url="/pages/"+options.originPageName+"/"+options.originPageName;
        this.setData({
            backUrl:url,
            isTab:options.isTab||'true'
        });
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
