import Helper from "./../../utils/util";

import HttpClientHelper from './../../utils/clinetHttpHelper';

let httpClientHelper = new HttpClientHelper.ClientHttpHelper();

import httpServer from './../../utils/serverCloud';

Page({
    /**
     * 页面的初始数据
     */
    data: {
        orderInfo: {
            startTime: 0,
            startDate: 0,
            endTime: 0,
            endDate: 0,
            timeLength: 2,
            totalPrice: 0,
            gid: 0,
            shouldPay: 0,
            isRunning: false,//订单是否提交
            orderNumber: ""
        },
        goodsInfo: {
            name: "",
            price: 2,
            imgUrl: "fdsa"
        },
    },
    addShowCar() {
        var that = this;
        if (this.data.orderInfo.isRunning) {
            wx.showToast({
                title: '已经添加到未付款订单！',
                icon: "success"
            });
            return;
        }

        wx.getStorage({
            key: 'uid',
            success: data => {
                var myDatas = that.constructParam();
                myDatas.uid = data.data;
                myDatas.orders_status = 2;
                console.log(myDatas)
                httpServer.get(httpServer.urls.addOrders, {
                    myDatas: myDatas
                })
                    .then(res => {
                        if (res.result.errMsg === "collection.add:ok") {
                            that.setData({
                                'orderInfo.isRunning': true
                            });
                            wx.showModal({
                                title: '物品已加入购物车！',
                                content: '可以去我的订单查看！',
                                success: res => {
                                    if (res.confirm) {

                                    }
                                }
                            })
                        }
                    })
                    .catch(err => {
                        wx.showModal({
                            title: '返回登录！',
                            content: '未登录状态',
                            success: res => {
                                if (res.confirm) {
                                    wx.redirectTo({
                                        url: '/pages/login/login'
                                    })
                                }
                            }
                        })
                    })
            }
        });
    },
    showMethods() {
        if (this.data.orderInfo.isRunning) {
            wx.showToast({
                title: '已经添加到未付款订单！',
                icon: "success"
            });
            return;
        }
        var that = this;
        wx.showActionSheet({
            itemList: ["微信", "支付宝"],
            success: res => {
                wx.getStorage({
                    key: 'uid',
                    success: data => {
                        var myDatas = that.constructParam();
                        myDatas.uid = data.data;
                        myDatas.orders_status = 1
                        httpServer.get(httpServer.urls.addOrders, {
                            myDatas: myDatas
                        })
                            .then(res => {
                                if (res.result.errMsg === "collection.add:ok") {
                                    that.setData({
                                        'orderInfo.isRunning': true
                                    });
                                    wx.showModal({
                                        title: '购买成功！',
                                        content: '可以去我的订单查看！',
                                        success: res => {
                                            if (res.confirm) {

                                            }
                                        }
                                    })
                                }
                            })
                            .catch(err => {
                                wx.showModal({
                                    title: '返回登录！',
                                    content: '未登录状态',
                                    success: res => {
                                        if (res.confirm) {
                                            wx.redirectTo({
                                                url: '/pages/login/login'
                                            })
                                        }
                                    }
                                })
                            })
                    }
                });
            }
        })
    },
    calculate() {
        var that = this.data;
        var start = that.orderInfo.startDate + " " + that.orderInfo.startTime;
        var end = that.orderInfo.endDate + " " + that.orderInfo.endTime;
        this.setData({
            'orderInfo.timeLength': Helper.calculateTime(start, end),
            "orderInfo.shouldPay": (Helper.calculateTime(start, end) * that.goodsInfo.price).toFixed(2)
        });
        console.log(that.orderInfo.timeLength * that.goodsInfo.price)
    },
    bindChange(e) {
        if (e.target.dataset.value === '11') {
            if (Helper.compare(e.detail.value, this.data.orderInfo.startTime,
                this.data.orderInfo.endDate, this.data.orderInfo.endTime)) {
                this.setData({
                    'orderInfo.startDate': e.detail.value
                });
            } else {
                wx.showToast({
                    title: '时间选择不正确！',
                    icon: "none"
                })
            }
        }
        if (e.target.dataset.value === '12') {
            if (Helper.compare(this.data.orderInfo.startDate, e.detail.value,
                this.data.orderInfo.endDate, this.data.orderInfo.endTime)) {
                this.setData({
                    'orderInfo.startTime': e.detail.value
                });
            } else {
                wx.showToast({
                    title: '时间选择不正确！',
                    icon: "none"
                })
            }
        }
        if (e.target.dataset.value === '21') {
            if (Helper.compare(this.data.orderInfo.startDate, this.data.orderInfo.startTime,
                e.detail.value, this.data.orderInfo.endTime)) {
                this.setData({
                    'orderInfo.endDate': e.detail.value
                });
            } else {
                wx.showToast({
                    title: '时间选择不正确！',
                    icon: "none"
                })
            }
        }
        if (e.target.dataset.value === '22') {
            if (Helper.compare(this.data.orderInfo.startDate, this.data.orderInfo.startTime,
                this.data.orderInfo.endDate, e.detail.value)) {
                this.setData({
                    'orderInfo.endTime': e.detail.value
                });
            } else {
                wx.showToast({
                    title: '时间选择不正确！',
                    icon: "none"

                })
            }

        }
        this.calculate();
    },

    constructParam() {
        var that = this;
        let params = {};
        var mydate = new Date();
        params.startTime = that.data.orderInfo.startDate + " " + that.data.orderInfo.startTime;
        params.endTime = that.data.orderInfo.endDate + " " + that.data.orderInfo.endTime;
        params.g_ur = that.data.goodsInfo.imgUrl;
        params.gname = that.data.goodsInfo.name;
        params.numbers = that.data.orderInfo.gname || 1;
        params.gid = that.data.orderInfo.gid;
        params.orderNumber = that.data.orderInfo.orderNumber;
        params.recordertime = Helper.formatTime(mydate);
        params.timeLength = parseInt(that.data.orderInfo.timeLength);
        params.per_price = that.data.goodsInfo.price;
        params.sum_price = parseInt(that.data.orderInfo.shouldPay);
        return params;
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {


        var that = this;
        var gid = options.id || "5c621e3f1b9d07166a61e05c";
        var date = new Date();
        this.setData({
            gid: gid,
            'orderInfo.startTime': Helper.myFormatTime(new Date(date.getTime() + 2 * 60 * 60 * 1000)),
            'orderInfo.startDate': Helper.myFormatDate(new Date(date.getTime() + 2 * 60 * 60 * 1000)),
            'orderInfo.endTime': Helper.myFormatTime(new Date(date.getTime() + (2 + that.data.orderInfo.timeLength) * 60 * 60 * 1000)),
            'orderInfo.endDate': Helper.myFormatDate(new Date(date.getTime() + (2 + that.data.orderInfo.timeLength) * 60 * 60 * 1000))
        });
        wx.getStorage({
            key: 'uid',
            success: res => {
                function upsetArr(arr) {
                    return arr.sort(function () {
                        return Math.random() - 0.5
                    });
                }

                this.setData({
                    'orderInfo.orderNumber': upsetArr((date.getTime() + res.data.substr(0, 5)).split("")).join("")
                });

            }
        });
        httpClientHelper.getSomeGoods(gid)
            .then(res => {
                var data = res.data[0];
                var url = data.imgs[0];

                if (!url.startsWith("http:")) {
                    url = 'http:' + url
                }
                that.setData({
                    'goodsInfo.name': data.title,
                    'goodsInfo.price': data.price,
                    'goodsInfo.imgUrl': url,
                });
                this.calculate();
            })
            .catch(function (err) {
                console.log(err)
            })


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
