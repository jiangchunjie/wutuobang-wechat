// pages/storage/storage.js
const mydatas = require("../../utils/datas");
const http = require("../../utils/serverCloud");
const urls = http.urls;
const get = http.get;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        scroll: {
            navs: [{
                name: "宠物",
                url: "/pages/storagePetsPage/storagePetsPage?",
                type:"pet"
            }, {
                name: "仓库",
                url: "/pages/storageCangkuPage/storageCangkuPage?",
                type:"warehouse"
            }, {
                name: "植物",
                url: "/pages/storagePlantsPage/storagePlantsPage?",
                type:"plant"
            }, {
                name: "仓库2",
                url: "/pages/storageCangkuPage/storageCangkuPage?",
                type:"pet"
            }],
            toView: "item1",
        },
        datas: [],
        dataOptions: {
            tempIndex: 0,
            perLength: 10,
            defaultLength: 14,
            isEnd: false,
            isLoading: false
        }
    },
    setViewData(e) {
        var that = this;
        this.setData({
            'scroll.toView': e.target.dataset.id,
            'dataOptions.tempIndex': 0,
            'dataOptions.isEnd': false,
            'dataOptions.isLoading': false,
            datas: []
        });
        this.loadingDatas()
    },
    goDetails(e) {
        var id = e.currentTarget.dataset.id;
        var index = this.data.scroll.toView.charAt(4);
        index = parseInt(index) - 1;
        var url = this.data.scroll.navs[index].url;
        url += "id=" + id;
        wx.navigateTo({
            url: url
        });
    },
    loadingDatas(isInit = false) {
        var that = this;
        //获取是哪一种类型
        var index = this.data.scroll.toView.charAt(4);
        index = parseInt(index) - 1;

        wx.showLoading({
            title: "loading",
            icon: "loading"
        });
        get(urls.getGoodsItems,
            {
                type: that.data.scroll.navs[index].type,
                perLength: that.data.dataOptions.perLength,
                tempPagesIndex: that.data.dataOptions.tempIndex
            })
            .then(function (res) {
                var datas = that.data.datas;
                datas = datas.concat(res.result.data)
                if (res.result.data&&res.result.data.length===0){
                    that.setData({
                        'dataOptions.isEnd': true
                    });
                }
                that.setData({
                    datas: datas,
                    // 'dataOptions.defaultLength': mydatas.plants.length
                });
                console.log(res.result.data)
                wx.hideLoading();
            });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        this.loadingDatas();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        var that=this;
        if (that.data.dataOptions.isLoading||that.data.dataOptions.isEnd) {
            return;
        }
        that.setData({
            'dataOptions.isLoading': true
        });

        if (!that.data.dataOptions.isEnd) {
            that.setData({
                "dataOptions.tempIndex":this.data.dataOptions.tempIndex+1
            });
            that.loadingDatas()
        }
        that.setData({
            'dataOptions.isLoading': false
        });
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
