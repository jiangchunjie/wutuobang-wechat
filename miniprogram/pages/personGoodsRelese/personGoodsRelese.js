import urls from './../../utils/datas';
import http from './../../utils/serverCloud';
import timeHelper from './../../utils/util';
import fileHelper from './../../utils/fileHelper';

Page({

    /**
     * 页面的初始数据
     */
    data: {
        name: "",
        phone: "",
        title: '',
        price: "",
        poi: {
            location: '',
            des: ''
        },
        typeDesc: {
            type: '',
            desc: '选择类型'
        },
        imgs: [],//当前文件
        urls: [],
        showUrls: false,
        keepData: false,
        isSubmit: false
    },
    submitImg(otherInfo) {
        if (this.data.isSubmit) {
            wx.showToast({
                title: '请勿重新提交！',
                icon: "none"
            });
            return;
        }
        this.setData({
            isSubmit: true
        });
        wx.showLoading();
        var that = this;
        var imgsUrl = [];
        var tempImg = this.data.imgs;
        var imgLength = tempImg.length;
        if (imgLength === 0) {
            wx.showToast({
                title: '请添加图片',
                icon: "none"
            });
            this.setData({
                isSubmit: false
            });
            return;
        }
        tempImg.map(function (item) {
            var houzui = item.path.substr(item.path.lastIndexOf("."));
            var name = new Date().getTime();
            that.uploadImg(name + houzui, item.path)
                .then(res => {
                    imgsUrl.push(res.fileID);
                    if (imgsUrl.length === imgLength) {
                        that.setData({
                            urls: imgsUrl
                        });
                        otherInfo.imgs = imgsUrl;
                        http.get(http.urls.goodsItems, {
                            params: otherInfo,
                            type: 'addGoods'
                        })
                            .then(res => {
                                if (res.result) {
                                    wx.showToast({
                                        title: '添加成功！',
                                        icon: 'none'
                                    })
                                } else {
                                    wx.showToast({
                                        title: '添加失败！',
                                        icon: 'none'
                                    });
                                    that.setData({
                                        isSubmit: false
                                    });
                                }
                                wx.hideLoading();
                            })
                            .catch(err => {
                                wx.showToast({
                                    title: '加载失败！',
                                    icon: 'none'
                                });
                                that.setData({
                                    isSubmit: false
                                });
                                wx.hideLoading();
                            })
                    }
                });

        });
    },
    uploadImg(aimPath, tempPath) {
        return fileHelper.upload(aimPath, tempPath)
    },
    //main
    confirmBasicInfo() {
        var that = this;
        var thatData = this.data;
        var param = {
            addr: thatData.poi.des,
            location: thatData.poi.location,
            comment: [],
            contact: thatData.name,
            id: new Date().getTime(),
            imgs: thatData.imgs,
            out_time: timeHelper.myFormatDate(new Date()),
            pn: thatData.phone,
            price: thatData.price,
            title: thatData.title,
            type: thatData.typeDesc.type,
            desc: thatData.typeDesc.desc,
        };
        wx.getStorage({
            key: 'uid',
            success: res => {
                var isOk = Object.keys(param).every((item, index, arr) => {
                    if(!param[item]){
                        wx.showToast({
                            title: '消息填写不完整！',
                            icon: "none"
                        });
                        return false;
                    }
                    //电话号码
                    if (item === 'pn') {
                        var reg = /^1[34578][0-9]{9}$/;
                        if (!reg.test(param[item])) {
                            wx.showToast({
                                title: '请填写正确的电话号码！',
                                icon: "none"
                            });
                            return false;
                        }
                    }
                    //价格
                    if (item==='price'){
                        if (parseFloat(param[item]).toString() === "NaN"){
                            wx.showToast({
                                title: '请填写正确的价格！',
                                icon: "none"
                            });
                            return false;
                        }
                    }
                    return true;
                });
                if (isOk) {
                    param.uid = res.data;
                    that.submitImg(param)
                }
            }
        });

    },
    infoChange(e) {
        var that = this;
        var type = e.currentTarget.dataset.type;
        var msg = e.detail.value;
        switch (type) {
            case 'name':
                that.setData({
                    name: msg
                });
                break;
            case 'phone':
                that.setData({
                    phone: msg
                });
                break;
            case 'title':
                that.setData({
                    title: msg
                });
                break;
            case 'price':
                that.setData({
                    price: msg
                });
                break;
            case 'desc':
                that.setData({
                    desc: msg
                });
                break;
        }
    },
    selectPos() {
        wx.redirectTo({
            url: '/pages/search/search?originPageName=personGoodsRelese&isTab=false'
        });
        this.setData({
            keepData: true
        })
    },
    setType(e) {
        var data = e.currentTarget.dataset;
        this.setData({
            'typeDesc.desc': data.desc,
            'typeDesc.type': data.type,
        });
        this.hideTypeModal();
    },
    showTypeModal() {
        this.setData({
            showUrls: true
        })
    },
    hideTypeModal() {
        this.setData({
            showUrls: false
        });
    },
    addImg(e) {
        var that = this;
        var length = this.data.imgs.length;
        if (length === 3) {
            wx.showToast({
                title: '最多上传三张照片！',
                icon: "none"
            });
            return;
        }
        wx.chooseImage({
            sizeType: ['compressed'],
            success: res => {
                var tempImg = that.data.imgs;
                that.setData({
                    imgs: tempImg.concat(res.tempFiles.splice(0, 3 - length))
                });
            }
        });
    },
    delImg(e) {
        var id = e.currentTarget.dataset.id;
        var data = this.data.imgs;
        data.splice(parseInt(id), 1);
        this.setData({
            imgs: data
        });
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        this.setData({
            urls: urls.title_urls
        });
        //返回加载数据
        if (options.searchResult) {
            wx.getStorage({
                key: 'tempData',
                success: res => {
                    if (res.data) {
                        var data = res.data;
                        that.setData({
                            name: data.name,
                            phone: data.phone,
                            title: data.title,
                            price: data.price,
                            'poi.location': data.poi.location,
                            'poi.des': data.poi.des,
                            "typeDesc.type": data.typeDesc.type,
                            "typeDesc.desc": data.typeDesc.desc,
                            imgs: data.imgs,//当前文件
                            urls: data.urls,
                        });
                    }
                    this.setData({
                        "poi.location": options.searchResult.split('|')[1],
                        "poi.des": options.searchResult.split('|')[0],
                    });
                }
            });


        }

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {
        var that = this.data;
        if (that.keepData) {
            var tempData = {
                name: that.name,
                phone: that.phone,
                title: that.title,
                price: that.price,
                poi: {
                    location: that.poi.location,
                    des: that.poi.des
                },
                typeDesc: {
                    type: that.typeDesc.type,
                    desc: that.typeDesc.desc
                },
                imgs: that.imgs,//当前文件
                urls: that.urls,
            };
            wx.setStorageSync('tempData', tempData);
        }
    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
