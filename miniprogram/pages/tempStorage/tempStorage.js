// pages/tempStorage/tempStorage.js
var amapFile = require('../../libs/amap-wx.js');
var temp = 0;
Page({

    /**
     * 页面的初始数据
     */
    data: {
        markers: [],
        latitude: '',
        longitude: '',
        textData: {},
        tempId: 0,
        polyline: "",
        tempMarkerLength: 0,
        mapOptions: {
            wayOfActiveIndex: 2,
            tempPosition: "北京",
        },
        paths: {
            path: [],
            distance: 0,
            duration: 0
        },
        itemList: [{
            title: "步行",
            describe: ""
        }, {
            title: "打车",
            describe: ""
        }, {
            title: "公交",
            describe: ""
        }, {
            title: "骑行",
            describe: ""
        }],
        //导航信息
        methodsItem: [],
        methodsItemDesc: {},
        myAmapFun: {},
        animations: {
            modalAnimationIn: null,
            mapAnimationIn: null
        }

    },
    chooseWay(e) {
        this.setData({
            mapOptions: {
                wayOfActiveIndex: e.target.dataset.way
            }
        })
    },
    closeModal() {
        var time = 300;
        var animation = wx.createAnimation({
            duration: time,
            timingFunction: 'ease'
        });
        animation.bottom("-60vh").step();
        this.setData({
            'animations.modalAnimationIn': animation.export()
        });
        setTimeout(function () {
            wx.showTabBar();
        }, time)
    },
    mapTap() {
        this.closeModal();
        var animation = wx.createAnimation({
            duration: 10,
            timingFunction: 'ease'
        });
        animation.height("60vh").step();
        this.setData({
            'animations.mapAnimationIn': animation.export()
        })
    },
    bodyTap(e) {
        this.closeModal();
        var animation = wx.createAnimation({
            duration: 10,
            timingFunction: 'ease'
        });
        animation.height("40vh").step();
        this.setData({
            'animations.mapAnimationIn': animation.export()
        })

    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var that = this;
        var myAmapFun = new amapFile.AMapWX({
            key: '1df6b408f60ad5c4f95d9fdb605dba47'
        });
        this.setData({
            myAmapFun: myAmapFun
        });
        wx.getLocation({
            type: 'gcj02',
            success: res => {
                that.setData({
                    latitude: res.latitude,
                    longitude: res.longitude
                });
                console.log({
                    latitude: res.latitude,
                    longitude: res.longitude
                })
                that.getdata();
            }, fail: function (res) {
                wx.showToast({
                    title: res.errMsg
                })
            }
        });
        myAmapFun.getRegeo({
            success: res => {
                that.setData({
                    'mapOptions.tempPosition': res[0].name
                });
                console.log(res);
            }
        })
    },
    //获取actionsheet的title数据
    getActionSheet() {
        var data = this.data.itemList;
        var list = [];
        data.forEach(function (item) {
            list.push(item.title)
        })
        return list;
    },
    drawPolyline(data) {
        var that = this;
        var points = [];
        console.log(data)
        if (data.paths && data.paths[0] && data.paths[0].steps) {
            // console.log(data)
            var steps = data.paths[0].steps;
            for (var i = 0; i < steps.length; i++) {
                var poLen = steps[i].polyline.split(';');
                for (var j = 0; j < poLen.length; j++) {
                    points.push({
                        longitude: parseFloat(poLen[j].split(',')[0]),
                        latitude: parseFloat(poLen[j].split(',')[1])
                    })
                }
            }

            data.paths[0].steps.forEach(function (item, index) {
                data.paths[0].steps[index].duration = (item.duration / 60).toFixed(1);
            });
            that.setData({
                polyline: [{
                    points: points,
                    color: "#0091ff",
                    width: 3
                }],
                paths: {
                    path: data.paths[0].steps,
                    distance: data.paths[0].distance,
                    duration: data.paths[0].duration
                }
            });
            console.log(that.data.paths)
        }
    },
    markerTab: function (e) {
        var that = this;
        var id = e.markerId;
        console.log(id)
        var data = that.data.markers;
        data.forEach(function (items, index) {
            delete data[index].iconPath;
        });
        data[id].iconPath = "../../assets/images/temp-map-marker-radius.png";
        that.setData({
            markers: data
        });
        var myAmapFun = new amapFile.AMapWX({
            key: '1df6b408f60ad5c4f95d9fdb605dba47'
        });
        var wayOfActiveIndex = that.data.mapOptions.wayOfActiveIndex;
        if (wayOfActiveIndex === 0) {
            myAmapFun.getWalkingRoute({
                origin: that.data.longitude + "," + that.data.latitude,
                destination: that.data.markers[id].longitude + "," + that.data.markers[id].latitude,
                success: function (data) {
                    that.drawPolyline(data);
                }
            });
        } else if (wayOfActiveIndex === 1) {
            myAmapFun.getDrivingRoute({
                origin: that.data.longitude + "," + that.data.latitude,
                destination: that.data.markers[id].longitude + "," + that.data.markers[id].latitude,
                success: function (data) {
                    that.drawPolyline(data);
                }
            })
        } else if (wayOfActiveIndex === 2) {

            var stategy = [
                "最快捷模式", "最经济模式", "最少换乘模式", "最少步行模式", "不乘地铁模式"
            ];
            wx.showActionSheet({
                itemList: stategy,
                success: res => {
                    myAmapFun.getTransitRoute({
                        origin: that.data.longitude + "," + that.data.latitude,
                        destination: that.data.markers[id].longitude + "," + that.data.markers[id].latitude,
                        city: '呼和浩特',
                        success: function (data) {
                            console.log(data)
                            if (data && data.transits) {
                                var transits = data.transits;
                                for (var i = 0; i < transits.length; i++) {
                                    var segments = transits[i].segments;
                                    transits[i].duration = (transits[i].duration / 60).toFixed(1);
                                    transits[i].transport = [];
                                    for (var j = 0; j < segments.length; j++) {
                                        var name = "";
                                        if (segments[j].bus && segments[j].bus.buslines) {
                                            var temp = "";
                                            for (var k = 0; k < segments[j].bus.buslines.length; k++) {
                                                if (segments[j].bus.buslines[k] && segments[j].bus.buslines[k].name) {
                                                    name = segments[j].bus.buslines[k].name.slice(0, segments[j].bus.buslines[k].name.indexOf("("));
                                                    if (k !== 0) {
                                                        temp += '/' + name;
                                                    } else {
                                                        temp = name;
                                                    }
                                                }
                                            }
                                            name = temp;
                                            if (name && 1) {
                                                if (j !== 0) {
                                                    // name = '-- ' + name;
                                                    transits[i].transport.push("--");
                                                }
                                                transits[i].transport.push(name);
                                            }
                                        }
                                    }
                                }
                            }
                            that.setData({
                                methodsItem: transits
                            });
                            wx.hideTabBar();
                            var animation = wx.createAnimation({
                                duration: 500,
                                timingFunction: "ease"
                            });
                            animation.bottom("0").step()
                            that.setData({
                                'animations.modalAnimationIn': animation.export()
                            })
                        }, fail: function (err) {
                            console.log(err)
                        }
                    })

                }
            })
        } else if (wayOfActiveIndex === 3) {
            myAmapFun.getRidingRoute({
                origin: that.data.longitude + "," + that.data.latitude,
                destination: that.data.markers[id].longitude + "," + that.data.markers[id].latitude,
                success: function (data) {
                    that.drawPolyline(data);
                }
            })
        }

    },
    getdata: function () {
        var that = this;
        var i = 1;
        var markers =[];
        var lenth = i + 6 + 1;//temp-map-marker
        markers.push({
            id: i-1 ,
            latitude: that.data.latitude,
            longitude: that.data.longitude,
            iconPath: "/assets/images/temp-map-marker.png"
        });
        while (i++ < lenth) {
            markers.push({
                id: i-1 ,
                latitude: that.data.latitude + Math.random() / 100,
                longitude: that.data.longitude + Math.random() / 100
            });
        }
        this.setData({
            markers: markers,
            tempMarkerLength: lenth
        });
        console.log(markers)
    },
    scanner() {
        wx.scanCode({
            success: res => {
                console.log(res)
            }
        });
    },
    search() {
        wx.navigateTo({
            url: "/pages/search/search?originPageName=tempStorage"
        })
    },
    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {
        var that = this;
        wx.getStorage({
            key: 'searchResult',
            success: res => {
                wx.setStorage({
                    key: "searchResult",
                    data: null
                });
                console.log()
                if (res.data && res.data.location) {
                    that.setData({
                        latitude: parseFloat(res.data.location.split(',')[1]),
                        longitude: parseFloat(res.data.location.split(',')[0]),
                        "mapOptions.tempPosition": res.data.name||that.data.mapOptions.tempPosition
                    });
                    that.getdata()
                }

            },
            fail: err => {
                console.log(err)
            }
        });
    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }


    //https://blog.csdn.net/jianggujin/article/details/72833711
})
