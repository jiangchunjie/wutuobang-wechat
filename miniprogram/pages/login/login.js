import ServerCloud from "./../../utils/serverCloud";


const app = getApp();

Page({

    /**
     * 页面的初始数据
     */
    data: {
        password: "",
        password2: '',
        isRegister: false
    },

    /**
     * 生命周期函数--监听页面加载
     */
    go() {
        var that = this;
        if (!((!this.data.isRegister && this.data.password) ||
            (this.data.isRegister && this.data.password && this.data.password2))) {
            wx.showToast({
                title: '请输入密码！',
                icon: "none",
                mask: true,
                duration: 1500
            });
            return;
        }
        // wx.showLoading();
        //注册
        if (this.data.isRegister) {
           if ( !(this.data.password === this.data.password2)) {
               wx.showToast({
                   title: '密码不相同！',
                   icon: "none",
                   mask: true,
                   duration: 1500
               });
               return;
           }
            ServerCloud.get(ServerCloud.urls.login, {
                type:'register',
                password: this.data.password
            })
                .then(res=>{
                    if (res.result&&res.result._id){
                        wx.showToast({
                            title: '注册成功！',
                            icon: "none",
                            mask: true,
                            duration: 1500
                        });
                        that.setData({
                            isRegister: false
                        })
                    }else{
                        wx.showToast({
                            title: '注册失败！',
                            icon: "none",
                            mask: true,
                            duration: 1500
                        });
                    }
                })
                .catch(err=>{
                    wx.showToast({
                        title: '系统错误！',
                        icon: "none",
                        mask: true,
                        duration: 1500
                    });
                });
           return;
        }
        //登陆
        ServerCloud.get(ServerCloud.urls.login, {
            password: this.data.password
        })
            .then(res => {
                wx.showToast({
                    title: res.result.msg,
                    icon: "none"
                });
                switch (res.result.status) {
                    case 1: //登陆成功
                        wx.setStorage({
                            key: "uid",
                            data: res.result.uid
                        });
                        console.log(wx.getStorageSync('uid'))
                        app.globalData.userId = res.result.uid;
                        wx.switchTab({
                            url: "/pages/home/home"
                        });
                        break;
                    case 0:

                        break;
                    case 2:
                        that.setData({
                            isRegister: true,
                            password: '',
                            password2: ''
                        });
                        break;
                }
            })
            .catch(err => {
                wx.showToast({
                    title: '网络错误！',
                    icon: "none",
                    mask: true,
                    duration: 1500
                });
                console.log(err)
            });
    },
    setPassword2(e) {
        this.setData({
            password2: e.detail.value,
        })
    },
    setPassword(e) {
        this.setData({
            password: e.detail.value,
        })
    },
    onLoad: function (options) {

    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
