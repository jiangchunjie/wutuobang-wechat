import HttpHelper from "./../../utils/clinetHttpHelper";
import mydata from "./../../utils/datas";

let httpHelper = new HttpHelper.ClientHttpHelper();
import serverCloud from "./../../utils/serverCloud"

Page({

    /**
     * 页面的初始数据
     */
    data: {
        mytabs: {
            toView: 'tab0'
        },
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        indicatorColor: "#fff",
        indicatorActiveColor: "#1BA160",

        types: [],
        param: {
            page: 1,
            per: 6,
            isOVer: false,
            type: 'all'
        },
        myGoods: []
    },
    //
    getGoods() {
        var that = this;
        var tempParam = this.data.param;
        wx.getStorage({
            key: 'uid',
            success: res => {
                httpHelper.getMyShopPageInfo(res.data, tempParam.type, tempParam.page, tempParam.per)
                    .then(res => {
                        that.setData({
                            "param.page": tempParam.page + 1,
                            myGoods: that.data.myGoods.concat(res.data)
                        });
                        if (res.data.length < tempParam.per) {
                            that.setData({
                                "param.isOver": true
                            })
                        }
                    })
                    .catch(err => {
                        console.log(err)
                    })
            }
        });
    },
    setViewData(e) {
        var that = this;
        var temp = this.data.mytabs.toView;
        var newToView = e.currentTarget.id;
        if (temp === newToView) {
            return;
        }
        this.setData({
            'mytabs.toView': e.currentTarget.id,
            'param.type': e.currentTarget.dataset.title,
            'param.page': 1,
            'param.isOver': false,
            myGoods: [],
        });
        this.getGoods();
    },
    //


    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
        var data = JSON.stringify(mydata.title_urls);
        data=JSON.parse(data);
        data.unshift({
            desc: "全部产品",
            title: "all",
        });
        this.setData({
            types: data
        });
        data = null;
        this.getGoods();
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {
        if (!this.data.param.isOVer) {
            this.getGoods();
        }
    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
