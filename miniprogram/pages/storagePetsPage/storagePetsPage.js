import HttpHelper from "./../../utils/clinetHttpHelper";

let httpHelper = new HttpHelper.ClientHttpHelper();
import serverCloud from "./../../utils/serverCloud"

Page({

    /**
     * 页面的初始数据
     */
    data: {
        mydata: {
            datas: [],
            id: "",
            type: ""
        },
        indicatorDots: true,
        autoplay: true,
        interval: 5000,
        duration: 1000,
        indicatorColor: "#fff",
        indicatorActiveColor: "#1BA160",
        tempObject: {
            isCollection: false
        }
    },

    order() {
        wx.navigateTo({
            url: '/pages/storageOrders/storageOrders?id=' + this.data.mydata.id
        })
    },

    changeCollectionStatus() {
        var that = this;
        wx.getStorage({
            key: 'uid',
            success: res => {
                serverCloud.get(serverCloud.urls.changeCollectionStatus, {
                    uid: res.data,
                    gid: that.data.tempObject.id
                }).then(res => {
                    console.log(res)
                }).catch(err => {
                    console.log(err)
                });

                this.setData({
                    'tempObject.isCollection': !this.data.tempObject.isCollection
                });
            },
            fail: function (err) {
                console.log(err)
            }
        });

    },
    isNumberInArr(num,arr){
        var flag=false;
        arr.forEach(function (item,index) {
            if (item===num) {
                flag=true;
            }
        });
        return flag;
    },
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {


        var id = options.id;
        var that = this;
        httpHelper.getSomeGoods(id)
            .then(res => {
                if ("conments" in res.data[0]) {
                    res.data[0].conments.forEach(function (index, item) {
                        if (index.photoUrl && !index.photoUrl.startsWith("http")) {
                            res.data[0].conments[item].photoUrl = "http:" + res.data[0].conments[item].photoUrl;
                        }
                    });
                }
                this.setData({
                    'mydata.datas': res.data,
                    'mydata.id': id,
                    'mydata.type': options.type,
                    tempObject: res.data[0]
                });

                wx.getStorage({
                    key: 'uid',
                    success: res => {
                        httpHelper.isCollection(res.data)
                            .then(res => {
                                that.setData({
                                    'tempObject.isCollection': that.isNumberInArr(that.data.tempObject.id , res.data[0].likes)
                                });
                            });
                    }
                });


            });


    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
