import HttpHelper from './../../utils/clinetHttpHelper'

let httpHelper = new HttpHelper.ClientHttpHelper();


Page({

    /**
     * 页面的初始数据
     */
    data: {
        activeIndex: 0,
        navBarOptions: {
            time: 1000,
            animate: null,
            tempTop: 0,
            duration: 800,
            isRunning: false
        },
        listOptions: {
            pageIndex: 0,
            per: 5,
            datas: [],
            isEnd:false,
            type:""
        }
    },

    /**
     * 生命周期函数--监听页面加载
     */
    loadingDatas(){
        wx.showLoading()
        var that = this;
        wx.getStorage({
            key: 'uid',
            success: res => {
                httpHelper.getOrdersList(res.data,that.data.listOptions.type,
                    that.data.listOptions.pageIndex,that.data.listOptions.per)
                    .then(res => {
                        that.setData({
                            'listOptions.datas': res.data
                        });
                        if (res.data.length<that.data.listOptions.per) {
                            that.setData({
                                'listOptions.isEnd': true
                            });
                        }
                        wx.hideLoading();
                        console.log( res.data)
                    })
                    .catch(err => {
                        wx.showToast({
                            title: '网络错误',
                            icon: "none"
                        });
                    });
            }
        });
    },
    switchNav(e) {
        this.setData({
            activeIndex: parseInt(e.target.dataset.id),
            'listOptions.type': parseInt(e.target.dataset.id),
            'listOptions.datas':[],
            'listOptions.isEnd': false,
        });
        this.loadingDatas()
    },
    onPageScroll: function (e) {
        var ani;
        var that = this;
        if (that.data.navBarOptions.isRunning) {
            return;
        }
        that.data.navBarOptions.isRunnin = true;
        ani = wx.createAnimation({
            duration: that.data.navBarOptions.duration,
            timingFunction: "ease"
        });
        if (parseInt(e.scrollTop) - that.data.navBarOptions.tempTop < 0) {
            ani.top("0").step();
            that.setData({
                'navBarOptions.animate': ani.export(),
                'navBarOptions.tempTop': parseInt(e.scrollTop)
            })
        } else {
            ani.top("-80rpx").step();
            that.setData({
                'navBarOptions.animate': ani.export(),
                'navBarOptions.tempTop': parseInt(e.scrollTop)
            })
        }
        setTimeout(function () {
            that.data.navBarOptions.isRunnin = false;
        }, that.data.navBarOptions.duration)


    },
    onLoad: function (options) {
        this.loadingDatas()
    },

    /**
     * 生命周期函数--监听页面初次渲染完成
     */
    onReady: function () {

    },

    /**
     * 生命周期函数--监听页面显示
     */
    onShow: function () {

    },

    /**
     * 生命周期函数--监听页面隐藏
     */
    onHide: function () {

    },

    /**
     * 生命周期函数--监听页面卸载
     */
    onUnload: function () {

    },

    /**
     * 页面相关事件处理函数--监听用户下拉动作
     */
    onPullDownRefresh: function () {

    },

    /**
     * 页面上拉触底事件的处理函数
     */
    onReachBottom: function () {

    },

    /**
     * 用户点击右上角分享
     */
    onShareAppMessage: function () {

    }
})
