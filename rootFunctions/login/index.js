// 云函数模板
// 部署：在 cloud-functions/login 文件夹右击选择 “上传并部署”

const cloud = require('wx-server-sdk')

// 初始化 cloud
cloud.init();
let database = cloud.database();
/**
 * 这个示例将经自动鉴权过的小程序用户 openid 返回给小程序端
 *
 * event 参数包含小程序端调用传入的 data
 *
 */
exports.main = (event, context) => {
    // console.log(event)
    // console.log(context)

    // 可执行其他自定义逻辑
    // console.log 的内容可以在云开发云函数调用日志查看

    // 获取 WX Context (微信调用上下文)，包括 OPENID、APPID、及 UNIONID（需满足 UNIONID 获取条件）
    const wxContext = cloud.getWXContext();
    var users = database.collection("users");

    //注册
    if (event.type === 'register') {
        return users.add({
            data:{
                addr:[],
                likes:[],
                openid: wxContext.OPENID,
                password:event.password
            }
        })
    }

    //登录
    return new Promise((resolve, reject) => {
        users.where({
            openid: wxContext.OPENID
        }).get()
            .then(res => {
                var result = res.data;
                // 0密码错误 1成功 2未注册
                if (result) {
                    if (result.length > 0) {
                        if (result[0].password === event.password) {
                            resolve({status: 1, msg: '登录成功', uid: result[0]._id});
                        } else {
                            resolve({status: 0, msg: '密码错误'})
                        }
                    } else {
                        resolve({status: 2, msg: '未注册'})
                    }
                }
            })
            .catch(err => {
                reject(err)
            })
    })


};
