// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
    let database = cloud.database()
    let users = database.collection("users");


    // return  users.doc(event.uid).get();

    return new  Promise((resolve,reject)=>{
        users.doc(event.uid).get()
            .then(res => {
                var idArr = res.data.likes;
                var index;
                if ((index = idArr.indexOf(event.gid)) >= 0) {
                    idArr.splice(index,1);
                } else {
                    idArr.push(event.gid)
                }
                users.doc(event.uid).update({
                    data: {
                        likes: idArr
                    }
                })
                    .then(res => {
                        resolve(res)
                    })
                    .catch(err=>{
                        reject(err)
                    })
            })
    })

}