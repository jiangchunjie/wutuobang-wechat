// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
let database=cloud.database();
let _=database.command;
// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  let user=database.collection("users");
  return user.doc(event.uid).update({
    data:{
      addrs:_.push(event.addr)
    }
  })
};
