// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
let database=cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
    return database.collection("orders").add({
        data:event.myDatas
    })
}