// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()

// 云函数入口函数
exports.main = async (event, context) => {
    // const wxContext = cloud.getWXContext();
    const db = cloud.database();

    var tempIndex=event.tempPagesIndex;
    var perLength=event.perLength;

    var goodsItems = db.collection("goodsItems").where({
        type:event.type
    }).skip(tempIndex*perLength).limit(perLength);

    return goodsItems.get()
};