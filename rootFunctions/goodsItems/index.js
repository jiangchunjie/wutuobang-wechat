// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init();
let database = cloud.database();
// 云函数入口函数
exports.main = async (event, context) => {
    const wxContext = cloud.getWXContext();

    //添加商品
    if (event.type === 'addGoods') {
        let goods = database.collection("goodsItems");
        return goods.add({
            data: event.params
        });
    }


};
